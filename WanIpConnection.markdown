This page is about LinuxIGD2 development branch which provides a reference implementation of WANIPConnection:2 service.
The service specification is here: [WANIPConnection:2](http://upnp.org/specs/gw/UPnP-gw-WANIPConnection-v2-Service.pdf).

The build and installation guide is here: [[WanIpConnectionInstallationGuide]]

Source code repository is here: [http://gitorious.org/igd2-for-linux/igd2-for-linux](http://gitorious.org/igd2-for-linux/igd2-for-linux)

