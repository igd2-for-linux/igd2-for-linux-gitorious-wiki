# OpenWrt + LinuxIGDv2 Installation Guide

Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, no Front-Cover Texts and
no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

See the Licenses page at http://www.gnu.org/ for more details.

This guide describes how to build and install OpenWRT and LinuxIGD2 UPnP gateway software into router hardware.
It also describes how to build LinuxIGD2 for Ubuntu workstation.
These instructions are verified with Ubuntu 10.04.

# Installing OpenWrt

OpenWrt has great Wiki-pages where installation of OpenWrt is
described for many type of devices. You can check if OpenWrt can be
installed for your device from here:
[http://oldwiki.openwrt.org/CompleteTableOfHardware.html](http://oldwiki.openwrt.org/CompleteTableOfHardware.html).

The target device is assumed to be Asus WL-500G Premium v.1. For more information about OpenWrt and WL-500G visit
[http://wiki.openwrt.org/oldwiki/openwrtdocs/hardware/asus/wl500gp](http://wiki.openwrt.org/oldwiki/openwrtdocs/hardware/asus/wl500gp)

Build your own image from source (instructions here:
[http://wiki.openwrt.org/doc/start\#building](http://wiki.openwrt.org/doc/start#building).
Use openwrt-brcm47xx-squashfs.trx image for flashing when using
TFTP or mtd.

## Step By Step

Here are listed the commands that need to be executed.

### Building Flash Image

    mkdir ~/openwrt
    cd ~/openwrt
    svn export -r21808 svn://svn.openwrt.org/openwrt/branches/backfire
    cd ~/openwrt/backfire
    ./scripts/feeds update
    make package/symlinks
    make menuconfig

Select *Broadcom BCM947xx/953xx* as Target System and Target
Profile (ASUS WL-500g Premium). Select *Exit*, then *Yes*.

    make download
    make world

If build succeeds firmware images will be located in
~/openwrt/backfire/bin/brcm47xx/ directory:

    ls -l bin/brcm47xx/openwrt-brcm47xx-squashfs.trx

### Install Image Using tftp client

This is the recommended method for first time installation.

First define a bash helper function by copy-pasting these lines
into bash prompt of Linux host:

    sudo apt-get install atftp

    flashrouter () {
        atftp --trace --option "timeout 60" --option "mode octet" --put --local-file $1 192.168.1.1
        date
        sleep 240 ;echo -e \\a # BEL
    }

Force the Asus router into flash mode by pressing black “RESTORE”
button when connecting power cable to the device. If the device
enters flash mode, it starts blinking its power led.

Check from Linux host, that you have connection to Asus:

    ping 192.168.1.1

Start flashing:

    cd ~/openwrt/backfire/bin/brcm47xx/
    flashrouter openwrt-brcm47xx-squashfs.trx

Wait three minutes while Asus stores the image into its flash
memory. **Do not power off Asus during that time!**After the time
has passed, power off/on the device, wati a couple of minutes, and
create telnet connection to Asus.

### Install Image Using the mtd command line tool

This method requires that OpenWRT is already installed and you want
to reflash the image. The following commands are executed in the
target device. It is also assumed that there is an ftp-server
offering target image file.

    cd /tmp
    wget <address of server>/openwrt-brcm-2.4-squashfs.trx
    mtd write /tmp/openwrt-brcm-2.4-squashfs.trx linux && reboot

### Commands after Image Installation

These commands must be executed after image is successfully
installed. (telnet and ssh commands are executed on pc connected to
router)

    telnet 192.168.1.1   
    passwd (Set desired password for the root user)
    exit

After these connection to device can be made with:

    ssh root@192.168.1.1

### Setting system time

It is useful, and almost mandatory that the system time is correct
in the system. The best and simplest way to automize system time
set up after every reboot is to use program called ntpclient.

Connect to device with:

    ssh root@192.168.1.1

Update package management and install nptclient:

    opkg update && opkg install ntpclient

Now the device will fetch time for the system after every reboot if
it does have internet connection. The default time zone which is
used is UTC, but if you want the system to use correct time zone do
the following (you can find at least some of the possible timezone
values for example from
[http://www.crisos.org/dokuwiki/doku.php?id=documentation:config:timezone](http://www.crisos.org/dokuwiki/doku.php?id=documentation:config:timezone)):

    uci set system.@system[0].zonename=Europe/Helsinki

    uci set system.@system[0].timezone=
    EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00

    uci commit system
    reboot

# Installing LinuxIGDv2

## Fetching sources from Gitorious

Clone the repository

    mkdir ~/linuxigd2
    cd ~/linuxigd2
    git clone git://gitorious.org/igd2-for-linux/igd2-for-linux.git

Prepare packages for OpenWRT.

    cd ~/linuxigd2/igd2-for-linux
    cp -ai pupnp_branch-1.6.x libupnp-1.6.6
    cd ~/linuxigd2/igd2-for-linux/libupnp-1.6.6
    autoreconf -v --install
    cd ~/linuxigd2/igd2-for-linux
    tar cvjf libupnp-1.6.6.tar.bz2 libupnp-1.6.6
    rm -rf libupnp-1.6.6

    cd ~/linuxigd2/igd2-for-linux/linuxigd2
    make dist

    mv ~/linuxigd2/igd2-for-linux/libupnp-1.6.6.tar.bz2 /tmp/libupnp-1.6.6.tar.bz2
    mv ~/linuxigd2/igd2-for-linux/linuxigd2/linuxigd2-0.3.tar.gz /tmp/linuxigd2-0.3.tar.gz

## Building LinuxIGDv2 for x86

If you have all the needed development libraries installed in your
compiling environment, all what is needed is to run make in the
LinuxIGDv2 source folder. After successful build the binary will be
located in ./bin/upnpd.

### Building and installing libupnp for x86

First we need to make sure that gnutls is installed:

    sudo apt-get install libgnutls26 libgnutls-dev

Build and install:

    cd ~/linuxigd2/igd2-for-linux/pupnp_branch-1.6.x
    autoreconf -v --install
    ./configure
    make
    sudo make install
    sudo ldconfig /usr/local/lib

If everything went fine, new modified upnp library should be
installed and usable by applications.

### Building and installing LinuxIGDv2 for x86

Untar LinuxIGDv2 archive, build and install it:

    tar -xzvf linuxigd2-1.0.tar.gz
    cd ~/linuxigd2/igd2-for-linux/linuxigd2
    make
    sudo make install

If everything went fine, LinuxIGDv2 can be started for example with
command:

    sudo upnpd –f eth0 eth0



## Building LinuxIGDv2 for OpenWrt

These instructions describe how to build own custom packages for
OpenWrt by using “custom feed” feature, which is described here
[http://wiki.openwrt.org/doc/howto/feeds](http://wiki.openwrt.org/doc/howto/feeds)
.

Building is done in the same directory as building of OpenWrt image
in chapter “[Building Image](#_Building_Image)”. All steps defined
in that chapter must be successfully executed. You also need some
extra packages from OpenWrt SVN, so execute the following commands
(NOTE: these commands are tested to work with Backfire branch
source, the detailed SVN revision is mentioned in chapter
“[Building Image](#_Building_Image)”).

As a preliminary work we need to fetch some required packages to
OpenWrt building environment:

    cd ~/openwrt/backfire
    ./scripts/feeds update packages



### Add a custom feed to OpenWRT

Announce the new custom feed directory to OpenWRT. Add this line to
the end of **~/openwrt/backfire/feeds.conf.default (**Note: Change
the username!)

    src-link customfeed /home/username/openwrt/customfeed

Extract customfeed directory which contains custom OpenWRT
Makefiles:

    cp -ai ~/linuxigd2/igd2-for-linux/openwrt/customfeed ~/openwrt
    cd ~/openwrt/backfire
    ./scripts/feeds update customfeed
    ./scripts/feeds install -p customfeed

New packages should be visible in the end of the list:

    ./scripts/feeds list

Install the new packages to OpenWRT:

    ./scripts/feeds install linuxigd2

### Build the packages

Tell to OpenWRT that new packages must be built as packages:

    make menuconfig

Select the new packages (by pressing \<M\>, when the component is
selected) from menu:

 * "libupnp" is under "Libraries" menu
 * “linuxigd2” is under “Network” menu

Select “*Exit”*, “*Exit”* and “*Yes”* =\> **.config** file is
updated in the current directory.

Copy the source packages (created above in chapter “[Fetching sources from Gitorious](#_Fetching_sources_from_Gitorious)”)
into ~/openwrt/backfire/dl directory:

    mv /tmp/libupnp-1.6.6.tar.bz2 ~/openwrt/backfire/dl
    mv /tmp/linuxigd2-0.3.tar.gz ~/openwrt/backfire/dl

Note! We are using currently our own modified version of libupnp.
We just copied it over the original package in dl directory.

All should be ready for building the packages. Execute following:

    make world V=99

After successful compilation installation packages should be found
in ~/openwrt/backfire/bin/brcm47xx/packages/ directory

### Updating LinuxIGDv2 source package for OpenWrt

If you have made changes to linuxigd2 sources, and you want to
create an updated source package for OpenWRT, it can be generated
like this (execute in linuxigd2 source folder):

    make dist

Copy the new source package into OpenWRT build system:

    cp <path>/linuxigd2-0.3.tar.gz ~/openwrt/backfire/dl

Rebuild only one package:

    make package/linuxigd2/compile V=99

## Installing Packages

Login to target device. Put installation packages (i.e. ipk
packages) into the ramdisk of target device (/tmp) e.g. with “scp”
command line command. Then execute the following commands:

    cd /tmp
    opkg update
    opkg install hostaplibs_0.1.0-1_mipsel.ipk
    opkg install libupnp_1.6.6-1_mipsel.ipk
    opkg install linuxigd2_1.1-1_mipsel.ipk

Opkg will now install all required packages into OpenWRT target
system.



# Using LinuxIGDv2

LinuxIGDv2 can be started with the following command:

    upnpd –f eth0.1 br-lan

Or as daemonized with:

    upnpd eth0.1 br-lan

Configuration file of LinuxIGDv2 is located in /etc/upnpd.conf and
XML-definition files are in /etc/linuxigd/.

Shutdown LinuxIGDv2 with either by pressing ctrl+C, or if
daemonized with command:

    killall upnpd

Control portmappings by using UPnP IGD control point or generic UPnP control point like these:
 - GUPnP Universal control point, [http://www.gupnp.org](http://www.gupnp.org).
 - UPnP Inspector [http://coherence.beebits.net/wiki/UPnP-Inspector](http://coherence.beebits.net/wiki/UPnP-Inspector)


# Change history

Version: 0.7
Date: 2009-05-07
Author: Jaakko Pasanen
Changes: Creating patched installation package of libupnp

Version: 0.8
Date: 2010-10-12
Author: Jukka Kinnunen
Changes: Lots of updates.

Version: 0.9
Date: 2010-10-14
Author: Kari Kallio, Jukka Kinnunen
Changes: Converted from word doc to markdown. Sources now fetched from Gitorious.

