# OpenWrt + LinuxIGDv2 with DeviceProtection Installation Guide

Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, no Front-Cover Texts and
no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".

See the Licenses page at http://www.gnu.org/ for more details.


----
# Installing OpenWrt

OpenWrt has great Wiki-pages where installation of OpenWrt is
described for many type of devices. You can check if OpenWrt can be
installed for your device from here:
[http://oldwiki.openwrt.org/CompleteTableOfHardware.html](http://oldwiki.openwrt.org/CompleteTableOfHardware.html).

From now on target device is assumed to be Asus WL-500G Premium
v.1. For more information about OpenWrt and WL-500G visit
[http://wiki.openwrt.org/oldwiki/openwrtdocs/hardware/asus/wl500gp](http://wiki.openwrt.org/oldwiki/openwrtdocs/hardware/asus/wl500gp)

Build your own image from source (instructions here:
[http://wiki.openwrt.org/doc/start#building](http://wiki.openwrt.org/doc/start#building).
Use openwrt-brcm47xx-squashfs.trx image for flashing when using TFTP or mtd.

----
## Step By Step

Here are listed the commands that need to be executed.

### Building Flash Image

Install packages required by OpenWRT

    sudo aptitude install build-essential binutils \
    binutils-dev flex bison autoconf gettext texinfo sharutils \
    subversion libncurses5-dev ncurses-term zlib1g-dev

Prepare build environment

    mkdir ~/openwrt
    cd ~/openwrt
    svn export -r21808 svn://svn.openwrt.org/openwrt/branches/backfire
    cd ~/openwrt/backfire
    ./scripts/feeds update
    make package/symlinks
    make menuconfig

Select "Target System (Broadcom BCM947xx/953xx)" and "Target Profile (ASUS WL-500g Premium v1 (Atheros WiFi))". Select *Exit*, then *Yes*.

    make download
    make world

If build succeeds, firmware images emerge in ~/openwrt/backfire/bin/brcm47xx/ directory:

    ls -l bin/brcm47xx/openwrt-brcm47xx-squashfs.trx


### Install Image to target HW Using tftp client

This is the recommended method for first time installation.

First define a bash helper function by copy-pasting these lines
into bash prompt of Linux host:

    sudo apt-get install atftp

    flashrouter () {
    atftp --trace --option "timeout 60" --option \
    "mode octet" --put --local-file $1 192.168.1.1
    date
    sleep 240 ;echo -e \\a # BEL
    }

Force the Asus router into flash mode by pressing black “RESTORE”
button when connecting power cable to the device. If the device
enters flash mode, it starts blinking its power led.

Check from Linux host, that you have connection to Asus:

    ping 192.168.1.1

Start flashing:

    cd ~/openwrt/backfire/bin/brcm47xx/
    flashrouter openwrt-brcm47xx-squashfs.trx

Wait three minutes while Asus stores the image into its flash
memory. **Do not power off Asus during that time!**After the time
has passed, power off/on the device, wati a couple of minutes, and
create telnet connection to Asus.



### Install Image Using the mtd command line tool

This optional method requires that OpenWRT is already installed and you want
to reflash the image. Following commands are executed at the target
device. Also assumes that there is ftp-server serving target image
file.

    cd /tmp
    wget <address of server>/openwrt-brcm-2.4-squashfs.trx
    mtd write /tmp/openwrt-brcm-2.4-squashfs.trx linux && reboot

### Commands after Image Installation

These commands must be executed after image is successfully
installed. (telnet and ssh commands are executed on pc connected to
router)

    telnet 192.168.1.1   
    passwd (Set desired password for the root user)
    exit

After these connection to device can be made with:

    ssh root@192.168.1.1

### Setting system time

It is usefull, and almost mandatory that the system time is correct
in the system. The best and simplest way to automize system time
set up after every reboot, is to use program called ntpclient.

Connect to target device with:

    ssh root@192.168.1.1

Update package management and install nptclient:

    opkg update && opkg install ntpclient

Now the device will fetch time for the system after every reboot if
it does have internet connection. The default time zone which is
used is UTC, but if you want the system to use correct time zone do
the following (you can find at least some of the possible timezone
values for example from
[http://www.crisos.org/dokuwiki/doku.php?id=documentation:config:timezone](http://www.crisos.org/dokuwiki/doku.php?id=documentation:config:timezone)):

    uci set system.@system[0].zonename=Europe/Helsinki

    uci set system.@system[0].timezone=
    EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00

    uci commit system
    reboot

----
# Installing LinuxIGDv2

LinuxIGDv2 with DeviceProtection-service needs new
libwpa_supplicant library and modified libupnp library. Here is
described how to build and install those libraries both for x86 (Ubuntu) and
for OpenWRT.

----
## Get the sources from Gitorious

Download the sources from GIT repository.
Note! Use the same path in development workstation, because it is referred later
in these instructions:

    mkdir -p ~/linuxigd2_test/src/ && cd ~/linuxigd2_test/src/
    git clone http://git.gitorious.org/igd2-for-linux/deviceprotection.git deviceprotection

----
## LinuxIGDv2 for x86 (Ubuntu)

The automated test script does the following tasks:

    * prepares development workstation, and installs all required packages
    * builds all components
    * prepares chrooted environment for linuxigd2
    * starts linuxigd2 and gupnp-universal-cp
    * runs some simple test cases to verify DeviceProtection

You can run the test script by issuing this command:

    cd ~/linuxigd2_test/src/deviceprotection/system_test/
    ./test.sh all

Note! The script requires sudo rights for some operations.
The system wide modifications are limited to package installation with "aptitude" command.
All other changes to the local filesystem happen in ~/linuxigd2_test directory.


----
## LinuxIGDv2 for OpenWrt

These instructions describe how to build own custom packages for
OpenWrt by using “custom feed” feature, which is described here
[http://wiki.openwrt.org/doc/howto/feeds](http://wiki.openwrt.org/doc/howto/feeds).

Building is done in the same directory as building of OpenWrt image
in chapter “[Building Flash Image](#_Building_Flash_Image)”. All steps defined
in that chapter must be successfully executed. You also need some
extra packages from OpenWrt SVN, so execute the following commands
(NOTE: these commands are tested to work with a specific OpenWRT Backfire branch
snapshot, the detailed SVN revision is mentioned in chapter
“[Building Flash Image](#_Building_Flash_Image)”).

As a preliminary work we need to fetch some required packages to
OpenWrt building environment:

    cd ~/openwrt/backfire
    ./scripts/feeds update packages
    ./scripts/feeds install libgnutls

### Add a custom feed to OpenWRT

Announce the new custom feed directory to OpenWRT. Add appropriate line to
the end of **~/openwrt/backfire/feeds.conf.default. The operation can be done
with this command:

    cat >>~/openwrt/backfire/feeds.conf.default <<EOF
    src-link customfeed /home/${USER}/openwrt/customfeed
    EOF

Copy customfeed directory which contains custom OpenWRT Makefiles:

    cp -ai ~/linuxigd2_test/src/deviceprotection/openwrt/customfeed/ ~/openwrt
    cd ~/openwrt/backfire
    ./scripts/feeds update customfeed
    ./scripts/feeds install -p customfeed

New packages names should be visible in the end of the list:

    ./scripts/feeds list

Install the new packages to OpenWRT:

    ./scripts/feeds install hostaplibs
    ./scripts/feeds install linuxigd2

### Modify the original libupnp Makefile

Overwrite the original OpenWRT Makefile for libupnp with our modified version:

    cp -a ~/linuxigd2_test/src/deviceprotection/openwrt/libupnp/Makefile \
    ~/openwrt/backfire/feeds/packages/libs/libupnp/Makefile

### Build the packages

Tell to OpenWRT that our new packages must be built as additional packages, and
not included into flashable image:

    make menuconfig

Select the new packages (by pressing <M>, when the component is
selected) from menu:

* "libupnp" is under "Libraries" menu
* "hostaplibs" is under "Libraries" menu
* “linuxigd2” is under “Network” menu

Select “*Exit”*, “*Exit”* and “*Yes”* => **.config** file is
updated in the current directory.

Create source archives and copy them into ~/openwrt/backfire/dl directory:

    rm ~/openwrt/backfire/dl/libupnp-*.tar.bz2
    cp -ai ~/linuxigd2_test/src/deviceprotection/pupnp_branch-1.6.x /tmp/libupnp-1.6.6
    tar -C /tmp -cvjf ~/openwrt/backfire/dl/libupnp-1.6.6.tar.bz2 libupnp-1.6.6
    rm -rf /tmp/libupnp-1.6.6

    rm ~/openwrt/backfire/dl/hostaplibs-*.tar.bz2
    cp -ai ~/linuxigd2_test/src/deviceprotection/hostap /tmp/hostaplibs-0.2.0
    tar -C /tmp -cvjf ~/openwrt/backfire/dl/hostaplibs-0.2.0.tar.bz2 hostaplibs-0.2.0
    rm -rf /tmp/hostaplibs-0.2.0

    rm ~/openwrt/backfire/dl/linuxigd2-*.tar.gz
    cd ~/linuxigd2_test/src/deviceprotection/linuxigd2
    make dist
    mv -i linuxigd2-0.8.tar.gz ~/openwrt/backfire/dl
    cd -

We are using our own modified version of libupnp. Eliminate original
libupnp package from build by editing **feeds/packages/libs/libupnp/Makefile** :

Change "PKG_VERSION:=1.6.12" => "PKG_VERSION:=1.6.6" so that we
build our modified version (which is currently based on older libupnp version)

All should be ready for building the packages. Execute the following commands:

    cd ~/openwrt/backfire
    make world V=99

After successful compilation installation packages should be located
in ~/openwrt/backfire/bin/brcm47xx/packages/ directory

----
## Installing Packages

Copy all required packages from development workstation to target device.
Use /tmp directory, which is in RAM:

    cd ~/openwrt/backfire/bin/brcm47xx/packages

    scp \
    libgpg-error_?.?-?_brcm47xx.ipk \
    libgcrypt_?.?.?-?_brcm47xx.ipk \
    hostaplibs_?.?.?-?_brcm47xx.ipk \
    libupnp_?.?.?-?_brcm47xx.ipk \
    dhcp-relay_?.?.?-?_brcm47xx.ipk \
    linuxigd2_?.?-?_brcm47xx.ipk \
    root@192.168.1.1:/tmp

Login to target device with ssh, and execute the following commands:

    cd /tmp
    opkg update

    opkg install \
    libgpg-error_?.?-?_brcm47xx.ipk \
    libgcrypt_?.?.?-?_brcm47xx.ipk \
    hostaplibs_?.?.?-?_brcm47xx.ipk \
    libupnp_?.?.?-?_brcm47xx.ipk \
    dhcp-relay_?.?.?-?_brcm47xx.ipk \
    linuxigd2_?.?-?_brcm47xx.ipk

    rm /tmp/*.ipk

All packages should be now installed in OpenWRT target system.

----
# Using LinuxIGDv2

LinuxIGDv2 is started with following command:

    upnpd –f eth0.1 br-lan

Or as daemonized with:

    upnpd eth0.1 br-lan

Settings file of LinuxIGDv2 is located at /etc/upnpd.conf and
XML-definition files are at /etc/linuxigd/.

Shutdown LinuxIGDv2 with either pressing ctrl+C, or if daemonized
with command:

    killall upnpd

----
# Hints and Tips

----
## Upnpd Error: UpnpStartHttpsServer returned -43

If this error is received after device reboot, it most likely is
caused by the system clock reset in reboot. If the server
certificate file /etc/certstore/libupnpX509server.pem has timestamp
older than current system time, the file is not accepted and error
is returned.

You can either remove the server certificate file and let the upnpd
to create new one on the next start up, or you can set the system
to set up the system time correctly after reboot as described in
chapter “[Setting system time](#_Setting_system_time)”.

----
# Change history

    Version: 0.8
    Date: 2011-03-09
    Author: Jukka Kinnunen
    Changes: Update to x86 build instructions. All can be done now with one script.

    Version: 0.7
    Date: 2011-03-09
    Author: Jukka Kinnunen
    Changes: Many corrections and updates to OpenWRT build instructions.

    Version: 0.6
    Date: 2011-03-08
    Author: Jukka Kinnunen
    Changes: Converted from word doc to markdown.

    Version: 0.5 Draft
    Date: 2011-02-11
    Author: Jukka Kinnunen
    Changes: Made the instructions version number agnostic.

    Version: 0.4 Draft
    Date: 2010-10-20
    Author: Jukka Kinnunen
    Changes: Minor fixes

    Version: 0.3 Draft
    Date: 2010-09-01
    Author: Jukka Kinnunen
    Changes:
        -OpenWRT updated to “backfire”.
        -“Custom feed” used.
        -Makefile listings omitted.
        -OpenWRT Makefile used for libupnp build.
        -Merged two 0.2 versions.

    Version: 0.2 Draft
    Date: 2009-10-09
    Author: Jaakko Pasanen
    Changes: Update instructions. Add [upnpd.init](#_unpnpd.init) and chapters 1.1.5 and 4.

    Version: 0.1 Draft
    Date: 2009-07-03
    Author: Jaakko Pasanen
    Changes: Installation instructions modified from OpenWRT_and_IGDv2_installation.doc 0.7 version

