# IGDv2 for Linux Implementation wiki
Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, no Front-Cover Texts and
no Back-Cover Texts. A copy of the license is included in the
section entitled "GNU Free Documentation License".

See the Licenses page at [http://gnu.org] (http://gnu.org) for more details.
	
# Introduction
	
IGDv2 for Linux is an updated version of Linux-IGD implementation [http://linux-igd.sourceforge.net/] (http://linux-igd.sourceforge.net). This new version is created on base of UPnP IGD:2 specifications available from [http://upnp.org/specs/gw/igd2] (http://upnp.org/specs/gw/igd2). Main changes are:

* Support for security via UPnP DeviceProtection service ([http://upnp.org/specs/gw/UPnP-gw-DeviceProtection-v1-Service.pdf] (http://upnp.org/specs/gw/UPnP-gw-DeviceProtection-v1-Service.pdf))
* Support for IPv6 firewall control via the new UPnP WANIPv6FirewallControl service ([http://upnp.org/specs/gw/UPnP-gw-WANIPv6FirewallControl-v1-Service.pdf] (http://upnp.org/specs/gw/UPnP-gw-WANIPv6FirewallControl-v1-Service.pdf))
* AddAnyPortMapping() action that allows proposing a port mapping, and if fails returns some available mapping back
* DeletePortMappingRange() allows removing a range of port mappings
* GetListOfPortMappings() returns a list of port mappings existing
* Enhanced eventing for port mapping status throug the SystemUpdateID state variable
* No infinite portmappings - maximum lease time is one week
